class CreateWeightRecord < ActiveRecord::Migration
  def change
    create_table :weight_records do |t|
      t.string :customer_info, index: true
      t.integer :weight

      t.timestamps null: false
    end
  end
end
