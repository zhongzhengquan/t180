FROM rails

RUN mkdir /opt/app
WORKDIR /opt/app

COPY . /opt/app
RUN bundle

RUN RAILS_ENV=production bundle exec rake db:create
RUN RAILS_ENV=production bundle exec rake db:migrate
RUN RAILS_ENV=production bundle exec rake assets:precompile

RUN echo "deb http://mirrors.aliyun.com/debian/ jessie main non-free contrib" > /etc/apt/sources.list && \
    sed -i '$a deb-src http://mirrors.aliyun.com/debian/ jessie main non-free contrib' /etc/apt/sources.list

RUN apt-get update && apt-get install -y vim
            
RUN touch ~/.vimrc && \
    sed -i '$a set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936' ~/.vimrc && \
    sed -i '$a set set termencoding=utf-8' ~/.vimrc && \
    sed -i '$a set set encoding=utf-8' ~/.vimrc

VOLUME ["/opt/app"]

EXPOSE 9292

CMD SECRET_KEY_BASE="3d0db1294a694272e2456fefbd73cb28b5c617e052e50066b787a321752f4cb83bb9e0499f715f82d0fd54e378acd659e7f14af6f8190f7cca3ec04fbc8afe5a" RAILS_ENV=production puma -C config/puma.rb
