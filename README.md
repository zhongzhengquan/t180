## 垃圾投放记录服务器端

### 系统环境
---

* rails 4.2.4
* ruby 2.0.0p451
* sqlite3

### 产品环境部署
---

1. bundle install
2. rake db:migrate RAILS_ENV=production
3. puma -C config/puma.rb
