$(document).ready(function(){
  setInterval(function() {
    $.get('/weight_records', function(data){
      $('table tbody').replaceWith(data);
    })
  }, 5000);

  $("#weight_records_customer_info").keypress(function(e){
    $this = $(this);

    if(e.which == 13){
      $.post("/weight_records", {"customer_info": $(this).val()}, function(data){
        $("#weight_records tbody").prepend(data);

        $this.val(''); // 清除输入框数据
        $(".error").text(''); // 清除错误提示
      }).fail(function(data, textStatus){
        $this.val(''); // 清除输入框数据
        $(".error").text(data.responseText); // 显示错误消息
      });
    }
  });
});
